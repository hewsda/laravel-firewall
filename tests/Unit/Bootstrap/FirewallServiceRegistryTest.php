<?php

declare(strict_types=1);

namespace FirewallTests\Unit\Bootstrap;

use FirewallTests\Unit\TestCase;
use Hewsda\Firewall\Bootstrap\FirewallServiceRegistry;

class FirewallServiceRegistryTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_be_constructed_with_empty_array()
    {
        $i = new FirewallServiceRegistry([]);

        $this->assertInstanceOf(FirewallServiceRegistry::class, $i);
    }

    /**
     * @test
     */
    public function it_can_add_bootstrap()
    {
        $i = new FirewallServiceRegistry([]);

        $this->assertCount(0, $i->bootstraps());

        $i->addRegistry(['foo', 0]);

        $this->assertCount(1, $i->bootstraps());
    }

    /**
     * @test
     */
    public function it_can_order_bootstraps_by_priority_and_return_flatten_array()
    {
        $b = [['foo', 10], ['bar', -10], ['foobar', -100]];

        $i = new FirewallServiceRegistry($b);

        $expected = ['foobar', 'bar', 'foo'];

        $this->assertSame($expected, $i->bootstraps());
    }

    /**
     * @test
     * @expectedException \Hewsda\Firewall\Exception\FirewallException
     */
    public function it_raise_exception_when_service_id_exists()
    {
        $this->expectExceptionMessage(
            sprintf('Firewall registry service "%s" already exists.', 'foo')
        );

        $i = new FirewallServiceRegistry([['foo', 10]]);
        $i->addRegistry(['foo', 0]);
    }

    /**
     * @test
     * @expectedException \Hewsda\Firewall\Exception\FirewallException
     */
    public function it_raise_exception_when_bootstrap_as_array_is_not_set_correctly()
    {
        $this->expectExceptionMessage('Bootstrap service must be an array of two members with service id and priority');

        new FirewallServiceRegistry([['foo']]);
    }

    /**
     * @test
     * @expectedException \Hewsda\Firewall\Exception\FirewallException
     */
    public function it_raise_exception_when_priority_is_not_an_integer()
    {
        $this->expectExceptionMessage(
            sprintf(
                'No priority set to service registry "%s"', 'foo')
        );

        new FirewallServiceRegistry([['foo','bar']]);
    }
}