<?php

declare(strict_types=1);

namespace FirewallTests\Unit\Foundation\Http\Exceptions;

use FirewallTests\Unit\TestCase;
use Hewsda\Firewall\Foundation\Http\Exceptions\AuthenticationExceptionHandler;
use Hewsda\Firewall\Foundation\Http\Exceptions\SecurityContextualExceptionHandler;
use Hewsda\Security\Core\Exception\AuthenticationException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationExceptionHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_return_response()
    {
        $request = $this->getMockBuilder(Request::class)->getMock();
        $context = $this->getMockBuilder(SecurityContextualExceptionHandler::class)
            ->disableOriginalConstructor()
            ->setMethods(['startAuthentication'])
            ->getMock();

        $context->expects($this->once())->method('startAuthentication')->willReturn(new Response('foo'));
        $exc = new AuthenticationException('bar');

        $m = new AuthenticationExceptionHandler();

        $this->assertInstanceOf(Response::class, $m->handle($exc, $request, $context));
    }
}