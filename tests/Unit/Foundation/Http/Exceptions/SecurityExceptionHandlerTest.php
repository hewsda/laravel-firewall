<?php

declare(strict_types=1);

namespace FirewallTests\Unit\Foundation\Http\Exceptions;

use FirewallTests\Unit\TestCase;
use Hewsda\Firewall\Foundation\Http\Exceptions\AuthenticationExceptionHandler;
use Hewsda\Firewall\Foundation\Http\Exceptions\AuthorizationExceptionHandler;
use Hewsda\Firewall\Foundation\Http\Exceptions\SecurityContextualExceptionHandler;
use Hewsda\Firewall\Foundation\Http\Exceptions\SecurityExceptionHandler;
use Hewsda\Security\Core\Exception\AccessDeniedException;
use Hewsda\Security\Core\Exception\AuthenticationException;
use Hewsda\Security\Core\Exception\SecurityException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityExceptionHandlerTest extends TestCase
{
    private $request;
    private $context;
    private $authentication;
    private $authorization;

    public function setUp()
    {
        $this->request = $this->getMockBuilder(Request::class)->getMock();

        $this->context = $this->getMockBuilder(SecurityContextualExceptionHandler::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->authentication = $this->getMockBuilder(AuthenticationExceptionHandler::class)
            ->disableOriginalConstructor()
            ->setMethods(['handle'])
            ->getMock();

        $this->authorization = $this->getMockBuilder(AuthorizationExceptionHandler::class)
            ->disableOriginalConstructor()
            ->setMethods(['handle'])
            ->getMock();
    }

    /**
     * @test
     */
    public function it_can_dispatch_authentication_exception()
    {
        $m = new SecurityExceptionHandler($this->context, $this->authentication, $this->authorization);
        $this->authentication->expects($this->once())->method('handle')->willReturn(new Response('ok'));
        $exc = new AuthenticationException('bar');

        $this->assertInstanceOf(Response::class, $m->handle($this->request, $exc));
    }

    /**
     * @test
     */
    public function it_can_dispatch_authorization_exception()
    {
        $m = new SecurityExceptionHandler($this->context, $this->authentication, $this->authorization);
        $this->authorization->expects($this->once())->method('handle')->willReturn(new Response('ok'));
        $exc = new AccessDeniedException('bar');

        $this->assertInstanceOf(Response::class, $m->handle($this->request, $exc));
    }

    /**
     * @test
     * @expectedException \FirewallTests\Unit\Foundation\Http\Exceptions\SomeSecurityException
     */
    public function it_raise_same_exception_when_not_handled()
    {
        $this->expectExceptionMessage('bar');

        $m = new SecurityExceptionHandler($this->context, $this->authentication, $this->authorization);
        $exc = new SomeSecurityException('bar');
        $m->handle($this->request, $exc);
    }
}

class SomeSecurityException extends SecurityException
{

}