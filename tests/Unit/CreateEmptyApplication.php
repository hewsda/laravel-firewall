<?php

namespace FirewallTests\Unit;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\Container as ContainerContract;
use Illuminate\Foundation\Application;

trait CreateEmptyApplication
{
    public function createEmptyApplication(): \Illuminate\Foundation\Application
    {
        $app = new Application();
        $app->bind(ContainerContract::class, Container::class);

        return $app;
    }
}