<?php

declare(strict_types=1);

namespace FirewallTests\Unit\Factory;

use FirewallTests\Unit\TestCase;
use Hewsda\Firewall\Factory\AuthenticationServices;
use Hewsda\Firewall\Factory\ServiceAware;
use Hewsda\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Hewsda\Security\Foundation\Value\ContextKey;

class ServiceAwareTest extends TestCase
{
    private $m;
    private $key;
    private $services;

    public function setUp()
    {
        $this->key = $this->getMockBuilder(ContextKey::class)
            ->disableOriginalConstructor()
            ->setMethods(['getKey'])
            ->getMock();

        $this->services = $this->getMockBuilder(AuthenticationServices::class)
            ->setMethods(['all'])
            ->getMock();

        $this->m = new ServiceAware(
            $this->key,
            $this->services,
            ['foo' => 'bar']
        );
    }

    /**
     * @test
     */
    public function it_can_access_firewall_name()
    {
        $this->assertSame($this->key, $this->m->name());
    }

    /**
     * @test
     */
    public function it_can_check_if_user_provider_key_exists()
    {
        $this->assertTrue($this->m->hasUserProvider('foo'));
    }

    /**
     * @test
     */
    public function it_can_fetch_user_provider_id()
    {
        $this->assertSame('bar', $this->m->userProvider('foo'));
    }

    /**
     * @test
     */
    public function it_can_access_user_providers_as_array()
    {
        $this->assertSame(['foo' => 'bar'], $this->m->userProviders());
    }


    /**
     * @test
     * @expectedException \Hewsda\Firewall\Exception\FirewallException
     */
    public function it_raise_exception_when_user_provider_key_does_not_exists()
    {
        $this->m->userProvider('foobar');
    }

    /**
     * @test
     */
    public function it_can_filter_services_by_given_position()
    {
        $factory = $this->getMockBuilder(AuthenticationServiceFactory::class)->getMock();
        $factory->expects($this->once())->method('position')->willReturn('some');
        $this->services->add($factory);

        $this->assertCount(1, $this->m->filterByPosition('some'));
    }

    /**
     * @test
     */
    public function it_can_return_an_empty_collection_when_position_does_not_match()
    {
        $factory = $this->getMockBuilder(AuthenticationServiceFactory::class)->getMock();
        $factory->expects($this->any())->method('position')->willReturn('foo');
        $this->services->add($factory);

        $this->assertTrue($this->m->filterByPosition('foobar')->isEmpty());
        $this->assertCount(0, $this->m->filterByPosition('bar'));
    }
}