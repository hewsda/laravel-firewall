<?php

declare(strict_types=1);

namespace FirewallTests\Unit\Factory\Payload;

use FirewallTests\Unit\TestCase;
use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\ServiceAggregate;

class ServiceAggregateTest extends TestCase
{
    private $m;

    public function setUp()
    {
        $this->m = $this->getMockBuilder(PayloadFactory::class)
            ->setMethods(['listener', 'provider', 'entrypoint'])
            ->getMock();
    }

    /**
     * @test
     */
    public function it_can_access_listeners()
    {
        $s = $this->getService();
        $this->m->expects($this->any())->method('listener')->willReturn('foo');
        $s($this->m);
        $this->assertSame(['foo'], $s->listeners());
    }

    /**
     * @test
     */
    public function it_can_access_providers()
    {
        $s = $this->getService();
        $this->m->expects($this->any())->method('provider')->willReturn('foo');
        $s($this->m);
        $this->assertSame(['foo'], $s->providers());
    }

    /**
     * @test
     */
    public function it_can_access_entrypoints()
    {
        $s = $this->getService();
        $this->m->expects($this->any())->method('entrypoint')->willReturn('foo');
        $s($this->m);
        $this->assertSame(['foo'], $s->entrypoints());
    }

    /**
     * @test
     */
    public function it_can_add_all_entries()
    {
        $s = $this->getService();
        $this->m->expects($this->any())->method('listener')->willReturn('foo');
        $this->m->expects($this->any())->method('provider')->willReturn('foo');
        $this->m->expects($this->any())->method('entrypoint')->willReturn('foo');

        $this->assertCount(0, $s->listeners());
        $this->assertCount(0, $s->providers());
        $this->assertCount(0, $s->entrypoints());

        $s($this->m);

        $this->assertCount(1, $s->listeners());
        $this->assertCount(1, $s->providers());
        $this->assertCount(1, $s->entrypoints());
    }

    private function getService()
    {
        return new ServiceAggregate();
    }
}