<?php

declare(strict_types=1);

namespace FirewallTests\Unit\Factory\Payload;

use FirewallTests\Unit\TestCase;
use Hewsda\Firewall\Factory\Payload\PayloadFactory;

class PayloadFactoryTest extends TestCase
{
    /**
     * @var PayloadFactory
     */
    private $i;

    public function setUp()
    {
        $this->i = new PayloadFactory();
    }

    /**
     * @test
     */
    public function it_can_access_listener()
    {
        $this->i->setListener('foo');
        $this->assertSame('foo', $this->i->listener());
    }

    /**
     * @test
     */
    public function it_can_access_provider()
    {
        $this->i->setProvider('foo');
        $this->assertSame('foo', $this->i->provider());
    }

    /**
     * @test
     */
    public function it_can_access_entrypoint()
    {
        $this->i->setEntrypoint('foo');
        $this->assertSame('foo', $this->i->entrypoint());
    }
}