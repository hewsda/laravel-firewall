<?php

declare(strict_types=1);

namespace FirewallTests\Unit\Factory\Payload;

use FirewallTests\Unit\TestCase;
use Hewsda\Firewall\Factory\Payload\PayloadService;
use Hewsda\Firewall\Foundation\Contracts\Factory\FirewallContext;
use Hewsda\Security\Foundation\Value\ProviderKey;

class PayloadServiceTest extends TestCase
{
    private $i;
    private $provider;
    private $context;

    public function setUp()
    {
        $this->provider = $this->getMockBuilder(ProviderKey::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder(FirewallContext::class)->getMock();

        $this->i = new PayloadService($this->provider, $this->context, 'some_provider', 'some_entrypoint');
    }

    /**
     * @test
     */
    public function it_can_access_provider_key()
    {
        $this->assertSame($this->provider, $this->i->firewallKey);
    }

    /**
     * @test
     */
    public function it_can_access_user_provider_id()
    {
        $this->assertSame('some_provider', $this->i->userProviderId);
    }

    /**
     * @test
     */
    public function it_can_access_firewall_context()
    {
        $this->assertSame($this->context, $this->i->context);
    }

    /**
     * @test
     */
    public function it_can_access_firewall_key_as_string_shortcut()
    {
        $this->provider->expects($this->once())->method('getKey')->willReturn('foo');

        $this->assertSame('foo', $this->i->firewallId());
    }
}