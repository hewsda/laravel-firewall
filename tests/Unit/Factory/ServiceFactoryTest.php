<?php

declare(strict_types=1);

namespace FirewallTests\Unit\Factory;

use FirewallTests\Unit\TestCase;
use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\ServiceAggregate;
use Hewsda\Firewall\Factory\ServiceAware;
use Hewsda\Firewall\Factory\ServiceFactory;
use Hewsda\Firewall\Foundation\Contracts\Factory\FirewallContext;
use Hewsda\Security\Foundation\Value\ContextKey;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Illuminate\Support\Collection;

class ServiceFactoryTest extends TestCase
{

    private $m;
    private $aware;
    private $context;

    public function setUp()
    {
        $this->aware = $this->getMockBuilder(ServiceAware::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->context = $this->getMockForAbstractClass(FirewallContext::class);
        $this->m = new ServiceFactory($this->aware, $this->context);
    }

    /**
     * @test
     */
    public function it_can_proxy_to_service_aware_to_filter_service_by_position()
    {
        $this->aware->expects($this->once())->method('filterByPosition')->willReturn(new Collection(['bar']));

        $this->assertEquals('bar', $this->m->make('bar')->first());
    }

    /**
     * @test
     */
    public function it_can_be_invoked_with_payload_to_aggregate_firewall()
    {
        $p = (new PayloadFactory())->setListener('foo');
        ($this->m)($p);
        $this->assertEquals(['foo'], $this->m->middleware());
    }

    /**
     * @test
     */
    public function it_can_set_http_request()
    {
        $this->markTestSkipped('To do:' . __FUNCTION__);
    }

    /**
     * @test
     */
    public function it_can_access_firewall_context()
    {
        $this->assertSame($this->context, $this->m->context());
    }

    /**
     * @test
     */
    public function it_can_access_service_aggregate()
    {
        $this->assertInstanceOf(ServiceAggregate::class, $this->m->aggregate());
    }

    /**
     * @test
     */
    public function it_can_access_listeners_as_middleware()
    {
        $p = (new PayloadFactory())->setListener('foo');
        ($this->m)($p);
        $this->assertSame(['foo'], $this->m->middleware());
    }

    /**
     * @test
     */
    public function it_can_check_if_default_context_key_is_same_as_firewall_context_key()
    {
        $c = new ContextKey('foo');

        $this->aware->expects($this->any())->method('name')->willReturn($c);
        $this->context->expects($this->any())->method('contextKey')->willReturn($c);
        $this->assertTrue($this->m->hasSameContext());
    }

    /**
     * @test
     */
    public function it_can_check_if_default_context_key_is_same_as_firewall_context_key_2()
    {
        $c = new ContextKey('foo');
        $p = new ContextKey('bar');

        $this->aware->expects($this->any())->method('name')->willReturn($c);
        $this->context->expects($this->any())->method('contextKey')->willReturn($p);
        $this->assertFalse($this->m->hasSameContext());
    }

    /**
     * @test
     */
    public function it_return_overriden_key_when_context_key_is_different_from_firewall_key_and_context_is_stateless()
    {
        $c = new ContextKey('foo');
        $p = new ContextKey('bar');

        $this->context->expects($this->once())->method('isStateless')->willReturn(true);
        $this->context->expects($this->any())->method('contextKey')->willReturn($p);
        $this->aware->expects($this->any())->method('name')->willReturn($c);

        $this->assertSame($c->getKey(), $this->m->key());
    }

    /**
     * @test
     */
    public function it_return_default_firewall_key_when_serialization_context_is_required()
    {
        $c = new ContextKey('foo');
        $p = new ContextKey('bar');

        $this->context->expects($this->any())->method('isStateless')->willReturn(false);
        $this->context->expects($this->any())->method('contextKey')->willReturn($p);
        $this->aware->expects($this->any())->method('name')->willReturn($c);

        $this->assertNotSame($c->getKey(), $this->m->key());
        $this->assertSame($p->getKey(), $this->m->key());
    }

    /**
     * @test
     */
    public function it_can_access_original_key_as_shortcut()
    {
        $c = new ContextKey('foo');
        $this->aware->expects($this->once())->method('name')->willReturn($c);

        $this->assertSame($c, $this->m->originalKey());
    }

    /**
     * @test
     */
    public function it_can_set_and_get_default_entrypoint()
    {
        $this->m->setDefaultEntrypoint('bar');

        $this->assertSame('bar', $this->m->defaultEntrypoint());
    }

    /**
     * @test
     */
    public function it_can_get_user_providers_as_array()
    {
        $this->aware->expects($this->once())->method('userProviders')->willReturn(['foo']);

        $this->assertSame(['foo'], $this->m->userProviderIds());
    }

    /**
     * @test
     */
    public function it_can_fetch_user_provider_id_by_overriden_key_from_authentication_service_factory()
    {
        $this->context->expects($this->never())->method('providerKey');
        $this->aware->expects($this->once())->method('userProvider')->willReturn('foo');

        $this->assertSame('foo', $this->m->userProvider('any'));
    }

    /**
     * @test
     */
    public function it_can_fetch_user_provider_from_firewall_context()
    {
        $p = new ProviderKey('foo');
        $this->context->expects($this->once())->method('providerKey')->willReturn($p);
        $this->aware->expects($this->once())->method('userProvider')->willReturn('foo');

        $this->assertSame('foo', $this->m->userProvider());
    }
}