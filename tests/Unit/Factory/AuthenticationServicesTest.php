<?php

declare(strict_types=1);

namespace FirewallTests\Unit\Factory;

use FirewallTests\Unit\TestCase;
use Hewsda\Firewall\Factory\AuthenticationServices;
use Hewsda\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Illuminate\Support\Collection;

class AuthenticationServicesTest extends TestCase
{
    public function it_can_be_constructed_with_null_value()
    {
        $i = new AuthenticationServices(null);
        $this->assertInstanceOf(AuthenticationServices::class, $i);
    }

    /**
     * @test
     */
    public function it_can_be_constructed_with_array_or_empty_array()
    {
        $i = new AuthenticationServices(['some_service']);
        $this->assertInstanceOf(AuthenticationServices::class, $i);

        $i = new AuthenticationServices([]);
        $this->assertInstanceOf(AuthenticationServices::class, $i);
    }

    /**
     * @test
     */
    public function it_can_return_services_as_collection()
    {
        $i = new AuthenticationServices(['some_service']);
        $this->assertInstanceOf(Collection::class, $i->all());
    }

    /**
     * @test
     */
    public function it_can_add_authentication_factory_service()
    {
        $factory = $this->getMockBuilder(AuthenticationServiceFactory::class)->getMock();
        $i = new AuthenticationServices();
        $this->assertCount(0, $i->all());

        $i->add($factory);
        $this->assertCount(1, $i->all());
    }
}