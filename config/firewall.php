<?php

return [

    'firewalls' => [],

    'user_providers' => [],

    'authorization' => [
        'role_hierarchy' => [
            'ROLE_ADMIN' => [
                'ROLE_USER'
            ]
        ],

        'strategy' => \Hewsda\Security\Core\Autorization\Strategy\UnanimousAuthorizationStrategy::class,
    ],
];