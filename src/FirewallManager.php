<?php

declare(strict_types=1);

namespace Hewsda\Firewall;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class FirewallManager
{
    /**
     * @var AuthenticationManager
     */
    private $manager;

    /**
     * @var FirewallDirector
     */
    private $director;

    /**
     * @var Collection
     */
    private $services;

    /**
     * FirewallManager constructor.
     *
     * @param AuthenticationManager $manager
     * @param FirewallDirector $director
     */
    public function __construct(AuthenticationManager $manager, FirewallDirector $director)
    {
        $this->manager = $manager;
        $this->director = $director;
        $this->services = new Collection();
    }

    public function processWithRequest(Request $request, array $middleware): Collection
    {
        foreach ($this->filterSecurityMiddleware($middleware) as $middlewareName) {
            $this->services->put($middlewareName, $this->manager->create($middlewareName));
        }

        return $this->director->process($this->services, $request);
    }

    private function filterSecurityMiddleware(array $middleware): array
    {
        return array_filter($middleware, function (string $name) {
            return $this->manager->hasFirewall($name);
        });
    }
}