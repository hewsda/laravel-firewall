<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Exception;

class FirewallException extends \RuntimeException implements FirewallExceptionContract
{
}