<?php

declare(strict_types=1);

namespace Hewsda\Firewall;

use Hewsda\Firewall\Exception\FirewallException;
use Hewsda\Firewall\Foundation\Contracts\Exception\ExceptionRegistry;
use Hewsda\Firewall\Foundation\Http\Events\FirewallHandled;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Routing\Events\RouteMatched;
use Illuminate\Routing\Router;

class Firewall
{
    /**
     * @var FirewallManager
     */
    private $firewallManager;

    /**
     * @var Router
     */
    private $router;


    /**
     * @var Dispatcher
     */
    private $events;

    /**
     * @var Container
     */
    private $container;

    /**
     * Firewall constructor.
     *
     * @param FirewallManager $firewallManager
     * @param Router $router
     * @param Dispatcher $dispatcher
     * @param Container $container
     */
    public function __construct(FirewallManager $firewallManager, Router $router, Dispatcher $dispatcher, Container $container)
    {
        $this->firewallManager = $firewallManager;
        $this->router = $router;
        $this->container = $container;
        $this->events = $dispatcher;
    }

    public function onRouteMatched(RouteMatched $event): void
    {
        $this->firewallManager
            ->processWithRequest($event->request, (array)$event->route->middleware())
            ->each(function (array $middleware, string $name) {
                $this->resolveExceptionHandler(array_pop($middleware));

                $this->setMiddlewareGroupToRouter($name, $middleware);
            });
    }

    private function setMiddlewareGroupToRouter(string $middlewareGroup, array $middleware): void
    {
        if ($this->router->hasMiddlewareGroup($middlewareGroup)) {
            foreach ($middleware as $middlewareName) {
                $this->router->pushMiddlewareToGroup($middlewareGroup, $middlewareName);
            }
        } else {
            $this->router->middlewareGroup($middlewareGroup, $middleware);
        }

        $this->events->dispatch(new FirewallHandled($middlewareGroup, $middleware));
    }

    private function resolveExceptionHandler(string $exceptionHandler): void
    {
        $exceptionListener =  $this->container->make($exceptionHandler);

        if(!$exceptionListener instanceof ExceptionRegistry){
            throw new FirewallException(
                sprintf('last middleware in firewall must implement %s', ExceptionRegistry::class));
        }

        $this->container->make(ExceptionHandler::class)
            ->setSecurityExceptionHandler($exceptionListener);
    }

    public function subscribe(Dispatcher $events): void
    {
        $events->listen(RouteMatched::class, [$this, 'onRouteMatched']);
    }
}