<?php

declare(strict_types=1);

namespace Hewsda\Firewall;

use Hewsda\Firewall\Exception\FirewallException;
use Hewsda\Firewall\Factory\AuthenticationServices;
use Hewsda\Firewall\Factory\ServiceFactory;
use Hewsda\Firewall\Foundation\Contracts\Factory\FirewallContext;
use Hewsda\Firewall\Foundation\Support\FirewallConfig;
use Hewsda\Security\Foundation\Value\ContextKey;
use Illuminate\Contracts\Container\Container;

class AuthenticationManager
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var FirewallConfig
     */
    private $firewallConfig;

    /**
     * @var array
     */
    private $firewall = [];

    /**
     * @var array
     */
    private $factories = [];

    /**
     * AuthenticationManager constructor.
     *
     * @param Container $container
     * @param FirewallConfig $firewallConfig
     */
    public function __construct(Container $container, FirewallConfig $firewallConfig)
    {
        $this->container = $container;
        $this->firewallConfig = $firewallConfig;
    }

    public function create(string $name): ServiceFactory
    {
        if (isset($this->firewall[$name])) {
            return $this->firewall[$name];
        }

        if (!$this->hasFirewall($name)) {
            throw new FirewallException(
                sprintf('Firewall name %s does not exists.', $name));
        }

        $config = $this->firewallConfig->get($name);

        return $this->firewall[$name] = $this->resolveFactory($name, $config);
    }

    public function extend(string $name, string $service, callable $factory): AuthenticationManager
    {
        $this->factories[$name][$service] = $factory;

        return $this;
    }

    private function resolveFactory(string $name, array $config): ServiceFactory
    {
        return new ServiceFactory(
            new ContextKey($name),
            $this->resolveServices($name, $config),
            $this->resolveContext($config['context']),
            $this->firewallConfig->getUserProviders()
        );
    }

    private function resolveServices(string $name, array $config): AuthenticationServices
    {
        $factories = new AuthenticationServices();

        foreach ((array)$config['services'] as $key => $service) {
            if (null !== $callback = array_get($this->factories, $name . '.' . $service)) {
                $factories->add(
                    is_callable($callback)
                        ? $callback($this->container)
                        : $this->container->make($callback)
                );
            }
        }

        if ($factories->all()->isEmpty()) {
            throw new FirewallException(
                sprintf('No service has been registered for firewall %s', $name));
        }

        return $factories;
    }

    private function resolveContext($context): FirewallContext
    {
        if (is_callable($context)) {
            if (!$this->container->bound(FirewallContext::class)) {
                throw new FirewallException(sprintf(
                        'Use a firewall context as callable must have have a default context class bound to interface "%s" in ioc',
                        FirewallContext::class)
                );
            }

            if ($this->container->isShared(FirewallContext::class)) {
                throw new FirewallException(sprintf(
                    '"%s" must not be shared in the ioc', FirewallContext::class
                ));
            }

            return $context($this->container->make(FirewallContext::class));
        }

        if (!class_exists($context)) {
            throw new FirewallException(
                sprintf('Firewall context class "%s" does not exists. ', $context));
        }

        return $this->container->make($context);
    }

    public function hasFirewall(string $name): bool
    {
        return $this->firewallConfig->hasFirewallName($name);
    }
}