<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Recaller;

use Hewsda\Firewall\Exception\FirewallException;
use Hewsda\Firewall\Factory\Payload\PayloadService;
use Hewsda\Security\Foundation\Value\RecallerKey;
use Hewsda\Security\Http\Recaller\SimpleRecallerService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Cookie\QueueingFactory;
use Illuminate\Contracts\Foundation\Application;

class RecallerManager
{
    /**
     * @var array
     */
    private $services = [];

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $factories = [];

    /**
     * RecallerManager constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function create(PayloadService $payload, string $listenerId, string $serviceKey): string
    {
        if (!$payload->context->hasRecallerForService($serviceKey)) {
            throw new FirewallException(
                sprintf('Recaller manager can not find any context for service key "%s" in configuration.', $serviceKey)
            );
        }

        $recallerKey = $payload->context->recaller($serviceKey);

        if ($this->hasService($recallerKey)) {
            return $this->services[$recallerKey->getKey()];
        }

        if (isset($this->factories[$serviceKey])) {
            $service = $this->factories[$serviceKey];
            return $this->services[$recallerKey->getKey()] = $this->registerExtender($payload, $listenerId, $serviceKey, $service);
        }

        return $this->services[$recallerKey->getKey()] = $this->createSimpleService($payload, $listenerId, $serviceKey);
    }

    protected function registerExtender(PayloadService $payload, string $listenerId, string $serviceKey, callable $service): string
    {
        $serviceId = $service($payload, $serviceKey);

        $this->registerOnResolving($listenerId, $serviceId);

        return $serviceId;
    }

    protected function createSimpleService(PayloadService $payload, string $listenerId, string $serviceKey): string
    {
        $recallerKey = $payload->context->recaller($serviceKey);
        $serviceId = 'firewall.remember_me.recaller_' . $recallerKey->getKey() . '_service.';

        $this->container->bind($serviceId, function (Application $app) use ($payload, $recallerKey) {
            return new SimpleRecallerService(
                $recallerKey,
                $payload->firewallKey,
                $app->make(QueueingFactory::class),
                $app->make($payload->userProviderId)
            );
        });

        $this->registerOnResolving($listenerId, $serviceId);

        return $serviceId;
    }

    protected function registerOnResolving(string $listenerId, $serviceId): void
    {
        $this->container->resolving($listenerId,
            function ($listener, Application $app) use ($serviceId) {
                if (!method_exists($listener, 'setRecaller')) {
                    throw new FirewallException(
                        sprintf('Class %s must implement "setRecaller" method.', get_class($listener))
                    );
                }

                $listener->setRecaller($app->make($serviceId));
            });
    }

    public function hasService(RecallerKey $recallerKey): bool
    {
        return array_key_exists($recallerKey->getKey(), $this->services);
    }

    public function getServiceId(RecallerKey $recallerKey): string
    {
        if ($this->hasService($recallerKey)) {
            return $this->services[$recallerKey->getKey()];
        }

        throw new FirewallException(
            sprintf('Recaller service with key %s not found in recaller manager.',
                $recallerKey->getKey())
        );
    }

    public function extend(string $serviceKey, callable $service): void
    {
        $this->factories[$serviceKey] = $service;
    }
}