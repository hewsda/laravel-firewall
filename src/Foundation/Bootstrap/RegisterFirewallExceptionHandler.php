<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Bootstrap;

use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\ServiceFactory;
use Hewsda\Firewall\Foundation\Http\Exceptions\AuthenticationExceptionHandler;
use Hewsda\Firewall\Foundation\Http\Exceptions\AuthorizationExceptionHandler;
use Hewsda\Firewall\Foundation\Http\Exceptions\SecurityContextualExceptionHandler;
use Hewsda\Firewall\Foundation\Http\Exceptions\SecurityExceptionHandler;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\TrustResolver;
use Hewsda\Security\Foundation\Contracts\Http\Authorization\AccessDenied;
use Hewsda\Security\Foundation\Contracts\Http\Entrypoint\Entrypoint;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;

class RegisterFirewallExceptionHandler
{
    /**
     * @var Container
     */
    private $container;

    /**
     * ExceptionRegistry constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(ServiceFactory $factory, \Closure $make)
    {
        $exceptionId = $this->registerExceptionListener($factory);

        $factory((new PayloadFactory())->setListener($exceptionId));

        $this->whenResolvingException(
            $exceptionId, $factory->aggregate()->entrypoints(), $factory->defaultEntrypoint());

        return $make($factory);
    }

    private function registerExceptionListener(ServiceFactory $factory): string
    {
        $id = 'firewall.exception_listener.' . $factory->key();

        $this->container->bind($id, function (Application $app) use ($factory) {
            return new SecurityExceptionHandler(
                $this->contextualExceptionInstance($factory),
                $app->make(AuthenticationExceptionHandler::class),
                $app->make(AuthorizationExceptionHandler::class)
            );
        });

        return $id;
    }

    private function contextualExceptionInstance(ServiceFactory $factory): SecurityContextualExceptionHandler
    {
        return new SecurityContextualExceptionHandler(
            new ProviderKey($factory->key()),
            $factory->context()->isStateless(),
            $this->container->make(TrustResolver::class),
            $this->resolveDefaultEntrypoint($factory->defaultEntrypoint()),
            $this->resolveDeniedHandler($factory->context()->deniedHandler())
        );
    }

    private function resolveDefaultEntrypoint(string $defaultEntrypoint = null): ?Entrypoint
    {
        if ($defaultEntrypoint) {
            return $this->container->make($defaultEntrypoint);
        }

        return null;
    }

    private function resolveDeniedHandler(string $deniedHandler = null): ? AccessDenied
    {
        if ($deniedHandler) {
            return $this->container->make($deniedHandler);
        }

        return null;
    }

    private function whenResolvingException(string $exceptionId, array $entrypoints, string $defaultEntrypoint = null): void
    {
        if (!$entrypoints) {
            return;
        }

        foreach ($entrypoints as $entrypoint) {
            if ($entrypoint !== $defaultEntrypoint) {
                $this->container->resolving($entrypoint, function (Entrypoint $entrypoint, Application $app) use ($exceptionId) {
                    $app->make($exceptionId)->setEntrypoint($entrypoint);
                });
            }
        }
    }
}