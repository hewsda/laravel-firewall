<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Bootstrap;

use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\ServiceFactory;
use Hewsda\Security\Core\Authentication\Provider\AnonymousAuthenticationProvider;
use Hewsda\Security\Foundation\Value\AnonymousKey;
use Hewsda\Security\Http\Firewall\AnonymousListener;
use Illuminate\Contracts\Container\Container;

class AuthorizeAnonymousRequest
{
    /**
     * @var Container
     */
    private $container;

    /**
     * AnonymousRegistry constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(ServiceFactory $factory, \Closure $make)
    {
        if ($factory->context()->isAnonymous()) {

            $listenerId = 'firewall.anonymous.default_authentication_listener.' . $factory->key();
            $providerId = 'firewall.anonymous.default_authentication_provider.' . $factory->key();
            $anonymousKey = $factory->context()->anonymousKey();

            $this->registerListener($listenerId, $anonymousKey);
            $this->registerProvider($providerId, $anonymousKey);

            $factory((new PayloadFactory())
                ->setProvider($providerId)
                ->setListener($listenerId)
            );
        }

        return $make($factory);
    }

    private function registerListener(string $listenerId, AnonymousKey $key): void
    {
        $this->container->bind($listenerId, function () use ($key) {
            return new AnonymousListener($key);
        });
    }

    private function registerProvider(string $providerId, AnonymousKey $key): void
    {
        $this->container->bind($providerId, function () use ($key) {
            return new AnonymousAuthenticationProvider($key);
        });
    }
}