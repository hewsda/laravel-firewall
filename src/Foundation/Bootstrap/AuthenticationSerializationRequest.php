<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Bootstrap;

use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\ServiceFactory;
use Hewsda\Security\Foundation\Value\ContextKey;
use Hewsda\Security\Http\Firewall\ContextListener;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;

class AuthenticationSerializationRequest
{
    /**
     * @var Container
     */
    private $container;

    /**
     * ContextRegistry constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(ServiceFactory $factory, \Closure $make)
    {
        if (!$factory->context()->isStateless()) {
            $listenerId = 'firewall.context.default_context_listener.' . $factory->key();

            $this->registerListener($listenerId, $factory);

            $factory((new PayloadFactory())->setListener($listenerId));
        }

        return $make($factory);
    }

    private function registerListener(string $listenerId, ServiceFactory $factory): void
    {
        $this->container->bind($listenerId, function (Application $app) use ($factory) {

            $providers = array_flatten($factory->userProviderIds());

            foreach ($providers as &$provider) {
                $provider = $this->container->make($provider);
            }

            return new ContextListener(
                new ContextKey($factory->key()),
                $app->make('events'),
                $providers
            );
        });
    }
}