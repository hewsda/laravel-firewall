<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Bootstrap;

use Hewsda\Firewall\Exception\FirewallException;
use Hewsda\Firewall\Factory\ServiceFactory;
use Hewsda\Security\Core\Authentication\AuthenticationProviders;
use Illuminate\Contracts\Container\Container;

class ConfigurationSetup
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var AuthenticationProviders
     */
    private $authenticationProviders;

    /**
     * BootstrapRegistry constructor.
     *
     * @param AuthenticationProviders $authenticationProviders
     * @param Container $container
     */
    public function __construct(AuthenticationProviders $authenticationProviders, Container $container)
    {
        $this->container = $container;
        $this->authenticationProviders = $authenticationProviders;
    }

    public function compose(ServiceFactory $factory, \Closure $make)
    {
        $this->registerDefaultEntrypoint($factory);

        $final = $make($factory);

        $this->makeAuthenticationProviders($factory->aggregate()->providers());

        return $final;
    }

    private function registerDefaultEntrypoint(ServiceFactory $factory): void
    {
        if (null !== $entryPoint = $factory->context()->entryPoint()) {

            $id = 'firewall.default_entry_point.' . $factory->key();

            $this->container->bindIf($id, $entryPoint);

            $factory->setDefaultEntrypoint($id);
        }
    }

    private function makeAuthenticationProviders(array $providers): void
    {
        if (empty($providers)) {
            throw new FirewallException(
                'You must add at least one authentication provider to authenticate');
        }

        foreach ($providers as $provider) {
            $this->authenticationProviders->addProvider($this->container->make($provider));
        }
    }
}