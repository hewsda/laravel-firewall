<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Bootstrap;

use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\Payload\PayloadService;
use Hewsda\Firewall\Factory\ServiceFactory;
use Hewsda\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Illuminate\Http\Request;

class RegisterAuthenticationServices
{
    /**
     * @var array
     */
    public static $positions = ['pre_auth', 'form', 'http', 'remember_me'];

    public function compose(ServiceFactory $factory, \Closure $make)
    {
        array_walk(self::$positions, function (string $position) use ($factory) {
            $factory
                ->make($position)
                ->map(function (AuthenticationServiceFactory $service) use ($factory) {
                    if ($this->isServiceRequired($service, $factory->request())) {
                        $this->registerBeforeService($service, $factory);

                        $factory($service->register($this->payload($service, $factory)));
                    }
                });
        });

        return $make($factory);
    }

    private function isServiceRequired(AuthenticationServiceFactory $service, Request $request): bool
    {
        if ($service->isRequired()) {
            return true;
        }

        if (null === $matcher = $service->matcher()) {
            return true;
        }

        return $matcher->matches($request);
    }

    private function payload(AuthenticationServiceFactory $service, ServiceFactory $factory): PayloadService
    {
        return new PayloadService(
            new ProviderKey($factory->key()),
            $factory->context(),
            $factory->userProvider($service->userProviderKey()),
            $factory->defaultEntrypoint()
        );
    }

    private function registerBeforeService(AuthenticationServiceFactory $service, ServiceFactory $factory): void
    {
        if (method_exists($service, 'before')) {

            $services = $service->before();

            foreach ((array)$services as $middleware) {
                $factory((new PayloadFactory())->setListener($middleware));
            }
        }
    }
}