<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Bootstrap;

use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\ServiceFactory;
use Hewsda\Firewall\Foundation\Logout\LogoutManager;

class RegisterLogoutServiceRequest
{
    /**
     * @var LogoutManager
     */
    private $logoutManager;

    /**
     * LogoutRegistry constructor.
     *
     * @param LogoutManager $logoutManager
     */
    public function __construct(LogoutManager $logoutManager)
    {
        $this->logoutManager = $logoutManager;
    }

    public function compose(ServiceFactory $factory, \Closure $make)
    {
        if (!$factory->context()->isStateless() && null !== $logout = $factory->context()->logout()) {

            $logoutId = $this->logoutManager->create($factory->originalKey(), $logout);

            $factory((new PayloadFactory())->setListener($logoutId));
        }

        return $make($factory);
    }
}