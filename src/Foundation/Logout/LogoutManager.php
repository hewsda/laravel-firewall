<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Logout;

use Hewsda\Firewall\Exception\FirewallException;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\TrustResolver;
use Hewsda\Security\Foundation\Value\ContextKey;
use Hewsda\Security\Http\Firewall\LogoutListener;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;

class LogoutManager
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $services = [];

    /**
     * LogoutManager constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function create(ContextKey $contextKey, string $logoutService): string
    {
        if ($this->hasService($contextKey)) {
            return $this->listenerId($contextKey);
        }

        $this->services[$contextKey->getKey()] = $this->resolveService($contextKey, $logoutService);

        return $this->listenerId($contextKey);
    }

    public function addHandler(ContextKey $contextKey, string $handler): void
    {
        if (!$this->hasService($contextKey)) {
            throw new FirewallException(
                sprintf('Add logout handler to context "%s" which is not found', $contextKey->getKey()));
        }

        $this->services[$contextKey->getKey()]->addHandler($handler);
    }

    protected function resolveService(ContextKey $key, string $logoutService): BaseLogoutService
    {
        $service = $this->container->make($logoutService);

        if (!is_subclass_of($service, BaseLogoutService::class)) {
            throw new FirewallException(
                sprintf('Logout service must extend %s', BaseLogoutService::class));
        }

        $this->{$this->getDriver($service)}($key, $service);

        return $service;
    }

    protected function registerDefaultLogoutListener(ContextKey $key, BaseLogoutService $service): void
    {
        if ($this->container->bound($this->listenerId($key))) {
            return;
        }

        $this->container->bindIf($this->listenerId($key), function (Application $app) use ($service) {
            return new LogoutListener(
                $app->make($service->successHandler()),
                $app->make(TrustResolver::class),
                $app->make($service->authenticationRequest())
            );
        });

        $this->container->resolving($this->listenerId($key), function (LogoutListener $listener, Application $app) use ($service) {
            foreach ($service->logoutHandlers() as $logoutHandler) {
                $listener->addHandler($app->make($logoutHandler));
            }
        });
    }

    protected function getDriver(BaseLogoutService $service): string
    {
        $driver = $service->driver();

        if (method_exists($this, $methodName = 'register' . ucfirst(camel_case($driver)) . 'LogoutListener')) {
            return $methodName;
        }

        throw new FirewallException(
            sprintf('Logout service driver "%s" not found.', $driver)
        );
    }

    public function listenerId(ContextKey $contextKey): string
    {
        return 'firewall.logout.default_logout_listener.' . $contextKey->getKey();
    }

    public function hasService(ContextKey $contextKey): bool
    {
        return isset($this->services[$contextKey->getKey()]);
    }
}