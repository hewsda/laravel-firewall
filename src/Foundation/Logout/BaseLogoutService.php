<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Logout;

abstract class BaseLogoutService
{
    /**
     * @var array
     */
    protected $logoutHandlers = [];

    /**
     * @var string
     */
    protected $successHandler;

    /**
     * @var string
     */
    protected $authenticationRequest;

    /**
     * @var string
     */
    protected $driver = 'default';

    public function addHandler(string $handlerId): void
    {
        $this->logoutHandlers[] = $handlerId;
    }

    public function setSuccessHandler(string $handler): void
    {
        $this->successHandler = $handler;
    }

    public function logoutHandlers(): ?array
    {
        return $this->logoutHandlers;
    }

    public function successHandler(): ?string
    {
        return $this->successHandler;
    }

    public function authenticationRequest(): ?string
    {
        return $this->authenticationRequest;
    }

    public function setAuthenticationRequest(string $authenticationRequest): void
    {
        $this->authenticationRequest = $authenticationRequest;
    }

    public function driver(): string
    {
        return $this->driver;
    }
}