<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Http\Events;

class FirewallHandled
{
    /**
     * @var array
     */
    private $middleware;

    /**
     * @var string
     */
    private $name;

    /**
     * FirewallHandled constructor.
     *
     * @param string $name
     * @param array $middleware
     */
    public function __construct(string $name, array $middleware)
    {
        $this->middleware = $middleware;
        $this->name = $name;
    }

    public function middleware(): array
    {
        return $this->middleware;
    }

    public function name(): string
    {
        return $this->name;
    }
}