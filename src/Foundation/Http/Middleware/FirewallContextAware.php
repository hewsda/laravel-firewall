<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Http\Middleware;

use Hewsda\Security\Foundation\Contracts\Core\Authentication\TrustResolver;
use Hewsda\Security\Foundation\HasToken;
use Hewsda\Security\Http\Event\ContextEventable;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\TerminableInterface;

class FirewallContextAware implements TerminableInterface
{
    use HasToken;

    /**
     * @var Dispatcher
     */
    private $events;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * @var ContextEventable
     */
    private $event;

    /**
     * FirewallContextAware constructor.
     *
     * @param Dispatcher $events
     * @param TrustResolver $trustResolver
     */
    public function __construct(Dispatcher $events, TrustResolver $trustResolver)
    {
        $this->events = $events;
        $this->trustResolver = $trustResolver;
    }

    public function handle(Request $request, \Closure $next)
    {
        $this->events->listen(ContextEventable::class, [$this, 'onEvent']);

        return $next($request);
    }

    public function onEvent(ContextEventable $event): void
    {
        if (null !== $this->event) {
            throw new \RuntimeException('Only one context can run per request.');
        }

        $this->event = $event;
    }

    public function terminate(SymfonyRequest $request, Response $response): void
    {
        if ($this->event) {

            $token = $this->token();

            if (null === $token
                || $this->trustResolver->isAnonymous($token)
                || $this->trustResolver->isTransitional($token)
            ) {
                $request->session()->forget($this->event->sessionKey);
            } else {
                $request->session()->put($this->event->sessionKey, serialize($token));
            }
        }
    }
}