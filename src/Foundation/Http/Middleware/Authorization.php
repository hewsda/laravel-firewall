<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Http\Middleware;

use Hewsda\Security\Foundation\Authorizer;
use Hewsda\Security\Foundation\Guard;
use Illuminate\Http\Request;

class Authorization
{
    use Guard, Authorizer;

    public function handle(Request $request, \Closure $next, ...$attributes)
    {
        $token = $this->requireToken();

        if (!$token->isAuthenticated()) {
            $this->setToken($token = $this->authenticate($token));
        }

        $this->requireGranted($token, $attributes, $request);

        return $next($request);
    }
}