<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Http\AuthenticationRequest;

use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationRequest;
use Illuminate\Http\Request as IlluminateRequest;
use Symfony\Component\HttpFoundation\Request;

class HttpBasicAuthenticationRequest implements AuthenticationRequest
{

    public function extract(IlluminateRequest $request): array
    {
        return [
            $request->headers->get('PHP_AUTH_USER'),
            $request->headers->get('PHP_AUTH_PW')
        ];
    }

    public function matches(Request $request): bool
    {
        return null === $request->headers->get('PHP_AUTH_USER');
    }
}