<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Http\Exceptions;

use Hewsda\Security\Core\Exception\AuthenticationException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationExceptionHandler
{
    public function handle(AuthenticationException $exception, Request $request, SecurityContextualExceptionHandler $contextualException): Response
    {
        try {
            return $contextualException->startAuthentication($request, $exception);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}