<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Http\Exceptions;

use Hewsda\Firewall\Foundation\Contracts\Exception\ExceptionRegistry;
use Hewsda\Security\Core\Exception\AccessDeniedException;
use Hewsda\Security\Core\Exception\AuthenticationException;
use Hewsda\Security\Core\Exception\SecurityException;
use Hewsda\Security\Foundation\Contracts\Http\Entrypoint\Entrypoint;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityExceptionHandler implements ExceptionRegistry
{
    /**
     * @var SecurityContextualExceptionHandler
     */
    private $contextualException;

    /**
     * @var AuthenticationExceptionHandler
     */
    private $authenticationExceptionHandler;

    /**
     * @var AuthorizationExceptionHandler
     */
    private $authorizationExceptionHandler;


    /**
     * SecurityExceptionHandler constructor.
     *
     * @param SecurityContextualExceptionHandler $contextualException
     * @param AuthenticationExceptionHandler $authenticationExceptionHandler
     * @param AuthorizationExceptionHandler $authorizationExceptionHandler
     */
    public function __construct(SecurityContextualExceptionHandler $contextualException, AuthenticationExceptionHandler $authenticationExceptionHandler, AuthorizationExceptionHandler $authorizationExceptionHandler)
    {
        $this->contextualException = $contextualException;
        $this->authenticationExceptionHandler = $authenticationExceptionHandler;
        $this->authorizationExceptionHandler = $authorizationExceptionHandler;
    }

    public function handle(Request $request, SecurityException $securityException): Response
    {
        if ($securityException instanceof AuthenticationException) {
            return $this->authenticationExceptionHandler->handle($securityException, $request, $this->contextualException);
        }

        if ($securityException instanceof AccessDeniedException) {
            return $this->authorizationExceptionHandler->handle($securityException, $request, $this->contextualException);
        }

        throw $securityException;
    }

    public function setEntrypoint(Entrypoint $entrypoint): void
    {
        $this->contextualException->setEntrypoint($entrypoint);
    }
}