<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Http\Exceptions;

use Hewsda\Security\Core\Exception\AuthenticationException;
use Hewsda\Security\Core\Exception\UserStatusException;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\TrustResolver;
use Hewsda\Security\Foundation\Contracts\Http\Authorization\AccessDenied;
use Hewsda\Security\Foundation\Contracts\Http\Entrypoint\Entrypoint;
use Hewsda\Security\Foundation\Guard;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityContextualExceptionHandler
{
    use Guard;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var bool
     */
    private $stateless;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * @var Entrypoint
     */
    private $entrypoint;

    /**
     * @var AccessDenied
     */
    private $accessDenied;

    /**
     * ContextualException constructor.
     *
     * @param ProviderKey $providerKey
     * @param bool $stateless
     * @param TrustResolver $trustResolver
     * @param Entrypoint|null $entrypoint
     * @param AccessDenied|null $accessDenied
     */
    public function __construct(ProviderKey $providerKey,
                                bool $stateless,
                                TrustResolver $trustResolver,
                                Entrypoint $entrypoint = null,
                                AccessDenied $accessDenied = null)
    {
        $this->providerKey = $providerKey;
        $this->stateless = $stateless;
        $this->trustResolver = $trustResolver;
        $this->entrypoint = $entrypoint;
        $this->accessDenied = $accessDenied;
    }

    public function startAuthentication(Request $request, AuthenticationException $exception): Response
    {
        if (!$this->entrypoint) {
            throw $exception;
        }

        if ($exception instanceof UserStatusException) {
            $this->eraseStorage();
        }

        return $this->entrypoint->start($request, $exception);
    }

    public function setEntrypoint(Entrypoint $entrypoint): SecurityContextualExceptionHandler
    {
        $this->entrypoint = $entrypoint;

        return $this;
    }

    public function setAccessDenied(AccessDenied $accessDenied): SecurityContextualExceptionHandler
    {
        $this->accessDenied = $accessDenied;

        return $this;
    }

    public function deniedHandler(): ?AccessDenied
    {
        return $this->accessDenied;
    }

    public function entrypointHandler(): ?Entrypoint
    {
        return $this->entrypoint;
    }
}