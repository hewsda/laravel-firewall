<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Http\Exceptions;

use Hewsda\Security\Core\Exception\AccessDeniedException;
use Hewsda\Security\Core\Exception\InsufficientAuthentication;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\TrustResolver;
use Hewsda\Security\Foundation\Guard;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthorizationExceptionHandler
{
    use Guard;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * AuthorizationExceptionHandler constructor.
     *
     * @param TrustResolver $trustResolver
     */
    public function __construct(TrustResolver $trustResolver)
    {
        $this->trustResolver = $trustResolver;
    }

    public function handle(AccessDeniedException $exception, Request $request, SecurityContextualExceptionHandler $contextualException): Response
    {
        // With default config, we added the recaller condition to avoid loop
        if (!$this->trustResolver->isFullFledged($this->token()) &&
            !$this->trustResolver->isRememberMe($this->token())
        ) {
            return $this->whenUserIsNotFullyAuthenticated($request, $exception, $contextualException);
        }

        return $this->whenUserIsNotGranted($request, $exception, $contextualException);
    }

    protected function whenUserIsNotFullyAuthenticated(Request $request, AccessDeniedException $exception, SecurityContextualExceptionHandler $contextual): Response
    {
        $message = 'Access denied, the user is not fully authenticated; redirecting to authentication entry point.';

        logger($message, ['exception' => $exception]);

        try {
            $message = 'Full authentication is required to access this resource.';
            $authException = new InsufficientAuthentication($message, 0, $exception);

            //$authException->setToken($this->token());

            return $contextual->startAuthentication($request, $authException);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function whenUserIsNotGranted(Request $request, AccessDeniedException $exception, SecurityContextualExceptionHandler $contextual): Response
    {
        logger('Access denied, the user is neither anonymous, nor remember-me.',
            ['exception' => $exception->getMessage()]);

        try {
            if (null !== $denied = $contextual->deniedHandler()) {
                return $denied->handle($request, $exception);
            }
        } catch (\Exception $e) {
            $message = 'An exception was thrown when handling an AccessDeniedException.';
            logger($message, ['exception' => $e->getMessage()]);
            throw $e;
        }

        throw $exception;
    }
}