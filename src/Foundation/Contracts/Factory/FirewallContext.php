<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Contracts\Factory;

use Hewsda\Security\Foundation\Value\AnonymousKey;
use Hewsda\Security\Foundation\Value\ContextKey;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Hewsda\Security\Foundation\Value\RecallerKey;

interface FirewallContext
{
    public function anonymousKey(): AnonymousKey;

    public function setAnonymousKey(string $anonymousKey): FirewallContext;

    public function providerKey(): ProviderKey;

    public function setProviderKey(string $providerKey): FirewallContext;

    public function isAnonymous(): bool;

    public function setAnonymous(bool $anonymous): FirewallContext;

    public function isStateless(): bool;

    public function setStateless(bool $stateless): FirewallContext;

    public function contextKey(): ContextKey;

    public function setContextKey(string $contextKey): FirewallContext;

    public function entryPoint(): ?string;

    public function setEntryPoint(string $entryPoint): FirewallContext;

    public function deniedHandler(): ?string;

    public function setDeniedHandler(string $deniedHandler): FirewallContext;

    public function logout();

    public function setLogout($logout): FirewallContext;

    public function switchUser();

    public function setSwitchUser(array $config): FirewallContext;

    public function recaller(string $serviceKey): ?RecallerKey;

    public function setRecaller(array $recaller): FirewallContext;

    public function hasRecallerForService(string $serviceKey): bool;
}