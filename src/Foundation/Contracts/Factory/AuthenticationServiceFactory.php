<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Contracts\Factory;

use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\Payload\PayloadService;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

interface AuthenticationServiceFactory
{
    public function register(PayloadService $payload): PayloadFactory;

    public function position(): string;

    public function key(): string;

    public function userProviderKey(): ?string;

    public function matcher(): ?RequestMatcherInterface;

    public function isRequired(): bool;
}