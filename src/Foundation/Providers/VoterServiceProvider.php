<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Providers;

use Illuminate\Support\ServiceProvider;
use Hewsda\Security\Core\Autorization\Voter\AnonymousVoter;
use Hewsda\Security\Core\Autorization\Voter\AuthenticatedVoter;
use Hewsda\Security\Core\Autorization\Voter\RoleHierarchyVoter;
use Hewsda\Security\Foundation\Contracts\Core\Role\RoleHierarchy;
use Illuminate\Contracts\Foundation\Application;

class VoterServiceProvider extends ServiceProvider
{
    const TAG_VOTERS = 'firewall.authorization.voters';

    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @var array
     */
    protected $voters = [
        AuthenticatedVoter::class,
        AnonymousVoter::class,
        RoleHierarchyVoter::class,
    ];

    public function register(): void
    {
        $this->registerVoters();

        $this->app->tag($this->voters, self::TAG_VOTERS);
    }

    public function provides(): array
    {
        return ['firewall.authorization.voters'];
    }

    protected function registerVoters(): void
    {
        $this->app->bind(RoleHierarchyVoter::class, function (Application $app) {
            return new RoleHierarchyVoter(
                $app->make(RoleHierarchy::class),
                'ROLE_'
            );
        });
    }
}