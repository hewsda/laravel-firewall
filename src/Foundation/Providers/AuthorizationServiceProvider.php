<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Providers;

use Hewsda\Firewall\Exception\FirewallException;
use Hewsda\Security\Core\Autorization\AuthorizationChecker;
use Hewsda\Security\Core\Role\RoleHierarchy;
use Hewsda\Security\Foundation\Contracts\Core\Authorization\Authorizable;
use Hewsda\Security\Foundation\Contracts\Core\Authorization\Grantable;
use Hewsda\Security\Foundation\Contracts\Core\Role\RoleHierarchy as RoleHierarchyContract;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AuthorizationServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    public function register(): void
    {
        $this->registerAuthorizationChecker();

        $this->registerRoleHierarchy();

        $this->registerAuthorizationStrategy();
    }

    public function provides(): array
    {
        return [
            Grantable::class,
            RoleHierarchyContract::class,
            Authorizable::class
        ];
    }

    private function registerAuthorizationChecker(): void
    {
        $this->app->bind(Grantable::class, AuthorizationChecker::class);
    }

    private function registerRoleHierarchy(): void
    {
        $this->app->bind(RoleHierarchyContract::class, function () {
            return new RoleHierarchy(config('firewall.authorization.role_hierarchy'));
        });
    }

    private function registerAuthorizationStrategy(): void
    {
        $strategy = config('firewall.authorization.strategy');

        if(!$strategy){
            throw new FirewallException('Missing firewall authorization strategy in configuration.');
        }

        if (!class_exists($strategy)) {
            throw new \InvalidArgumentException(sprintf(
                'Authorization strategy class "%s", defined in firewall config, does not exists.', $strategy
            ));
        }

        $this->app->bind(Authorizable::class, function (Application $app) use ($strategy) {
            return new $strategy($app->tagged('firewall.authorization.voters'));
        });
    }
}