<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Providers;

use Illuminate\Support\ServiceProvider;
use Hewsda\Security\Core\Authentication\Authentication;
use Hewsda\Security\Core\Authentication\AuthenticationProviders;
use Hewsda\Security\Core\Authentication\GenericTrustResolver;
use Hewsda\Security\Core\Authentication\Token\AnonymousToken;
use Hewsda\Security\Core\Authentication\Token\RecallerToken;
use Hewsda\Security\Core\Authentication\Token\Storage\TokenStorage;
use Hewsda\Security\Core\User\UserChecker;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\Authenticatable;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\Token\TokenStorage as Storage;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\TrustResolver;
use Hewsda\Security\Foundation\Contracts\Core\User\UserChecker as UserCheckerContract;

class SecurityServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    public function register(): void
    {
        $this->app->singleton(Storage::class, TokenStorage::class);

        $this->app->bind(TrustResolver::class, function () {
            return new GenericTrustResolver(AnonymousToken::class, RecallerToken::class);
        });

        $this->app->bind(UserCheckerContract::class, UserChecker::class);

        $this->app->singleton(AuthenticationProviders::class);

        $this->app->singleton(Authenticatable::class, Authentication::class);
    }

    public function provides(): array
    {
        return [
            Storage::class,
            TrustResolver::class,
            UserCheckerContract::class,
            AuthenticationProviders::class,
            Authenticatable::class
        ];
    }
}