<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Providers;

use Illuminate\Support\AggregateServiceProvider;

class AuthenticationFactoryServiceProvider extends AggregateServiceProvider
{
    protected $providers = [];
}