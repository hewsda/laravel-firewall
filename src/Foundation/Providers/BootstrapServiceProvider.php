<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Providers;

use Hewsda\Firewall\Bootstrap\FirewallServiceRegistry;
use Hewsda\Firewall\Foundation\Bootstrap\AuthenticationSerializationRequest;
use Hewsda\Firewall\Foundation\Bootstrap\AuthorizeAnonymousRequest;
use Hewsda\Firewall\Foundation\Bootstrap\ConfigurationSetup;
use Hewsda\Firewall\Foundation\Bootstrap\RegisterAuthenticationServices;
use Hewsda\Firewall\Foundation\Bootstrap\RegisterFirewallExceptionHandler;
use Hewsda\Firewall\Foundation\Bootstrap\RegisterLogoutServiceRequest;
use Illuminate\Support\ServiceProvider;

class BootstrapServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @var array
     */
    protected $bootstraps = [
        [ConfigurationSetup::class, -100],
        [AuthenticationSerializationRequest::class, -50],
        [RegisterLogoutServiceRequest::class, -25],
        [RegisterAuthenticationServices::class, 0],
        [AuthorizeAnonymousRequest::class, 25],
        [RegisterFirewallExceptionHandler::class, 100]
    ];

    public function register(): void
    {
        $this->app->singleton(FirewallServiceRegistry::class, function () {
            return new FirewallServiceRegistry($this->bootstraps);
        });
    }

    public function provides(): array
    {
        return [FirewallServiceRegistry::class];
    }
}