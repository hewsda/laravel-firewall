<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Providers;

use Hewsda\Firewall\AuthenticationManager;
use Hewsda\Firewall\Firewall;
use Hewsda\Firewall\Foundation\Http\Middleware\FirewallContextAware;
use Hewsda\Firewall\Foundation\Logout\LogoutManager;
use Hewsda\Firewall\Foundation\Recaller\RecallerManager;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class FirewallServiceProvider extends ServiceProvider
{
    public function boot(Router $router): void
    {
        $this->app->singleton(FirewallContextAware::class);
        $router->prependMiddlewareToGroup('web', FirewallContextAware::class);

        $this->app->singleton(Firewall::class);
        $this->app['events']->subscribe(Firewall::class);
    }

    public function register(): void
    {
        $this->app->singleton(LogoutManager::class);

        $this->app->singleton(RecallerManager::class);

        $this->app->singleton(AuthenticationManager::class);
    }
}