<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Providers;

use Illuminate\Support\ServiceProvider;

class BladeDirectiveServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        \Blade::directive('isGranted', function ($expression) {
            return "<?php if (app(\Hewsda\\Security\\Foundation\\Contracts\\Core\\Authorization\\Grantable::class)->isGranted({$expression})): ?>";
        });

        \Blade::directive('isNotGranted', function ($expression) {
            return "<?php if (!app(\Hewsda\\Security\\Foundation\\Contracts\\Core\\Authorization\\Grantable::class)->isGranted({$expression})): ?>";
        });
    }
}