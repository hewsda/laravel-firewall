<?php

namespace Hewsda\Firewall\Foundation\Support;

use Hewsda\Security\Core\Exception\AccessDeniedException;
use Hewsda\Security\Core\Exception\AuthenticationCredentialsNotFound;
use Hewsda\Security\Foundation\Contracts\Core\Authorization\Grantable;
use Hewsda\Security\Foundation\Contracts\Core\User\User as UserContract;
use Hewsda\Security\Foundation\Guard;

trait Secure
{
    use Guard;

    private function grant(): Grantable
    {
        return app(Grantable::class);
    }

    protected function isGranted($attributes, $object = null): bool
    {
        return $this->grant()->isGranted((array)$attributes, $object);
    }

    protected function requireGranted($attributes, $object = null): bool
    {
        if (!$this->isGranted($attributes, $object)) {
            $this->raiseAccessDenied();
        }

        return true;
    }

    protected function user(): UserContract
    {
        if (null === $user = $this->token()->user()) {
            throw new AuthenticationCredentialsNotFound('No token found in storage.');
        }

        return $user;
    }

    protected function raiseAccessDenied(string $message = null): void
    {
        $message = $message ?? 'Access denied from Secure trait.';

        throw new AccessDeniedException($message);
    }
}