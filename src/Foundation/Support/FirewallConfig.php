<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Support;

use Hewsda\Firewall\Exception\FirewallException;

class FirewallConfig
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var string
     */
    private $namespace = 'firewalls';

    /**
     * FirewallConfig constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function get(string $key = null)
    {
        if (!$this->config) {
            throw new FirewallException('Firewall config not found.');
        }

        if (!$key) {
            return $this->config;
        }

        $key = $this->getNamespaceKey($key);
        $services = array_get($this->config, $key);

        if (null === $services) {
            throw new FirewallException(sprintf('Can not locate %s key in firewall configuration.', $key));
        }

        $this->validateServiceConfiguration(array_get($this->config, $key));

        return $services;
    }

    public function hasFirewallName(string $name): bool
    {
        return null !== array_get($this->config, $this->getNamespaceKey($name));
    }

    public function getUserProviders(): array
    {
        if (!isset($this->config['user_providers'])) {
            throw new FirewallException('Missing "user_providers" key in firewall configuration');
        }

        $providers = $this->config['user_providers'];

        if (!is_array($providers) || empty($providers)) {
            throw new FirewallException('User providers must be an array and cannot be empty');
        }

        return $providers;
    }

    protected function validateServiceConfiguration($service): void
    {
        if (!is_array($service)) {
            throw new FirewallException('Firewall service configuration must be an array.');
        }

        if (!isset($service['context'])) {
            throw new FirewallException('Missing "context" key in firewall configuration.');
        }

        if (!isset($service['services'])) {
            throw new FirewallException('Missing "services" key in firewall configuration.');
        }

        if (!is_array($service['services']) || empty($service['services'])) {
            throw new FirewallException(
                '"services" key from firewall configuration must be an array and must not be empty.');
        }
    }

    protected function getNamespaceKey(string $key): string
    {
        return $this->namespace . '.' . $key;
    }
}