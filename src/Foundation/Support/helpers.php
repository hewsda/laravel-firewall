<?php

if (!function_exists('getToken')) {

    function getToken(): \Hewsda\Security\Foundation\Contracts\Core\Authentication\Token\Tokenable
    {
        return app(\Hewsda\Security\Foundation\Contracts\Core\Authentication\Token\TokenStorage::class)
            ->getToken();
    }
}

if (!function_exists('authenticatedUser')) {

    function authenticatedUser()
    {
        return app(\Hewsda\Security\Foundation\Contracts\Core\Authentication\Token\TokenStorage::class)
            ->getToken()->user();
    }
}
