<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Foundation\Context;

use Hewsda\Firewall\Exception\FirewallException;
use Hewsda\Firewall\Foundation\Contracts\Factory\FirewallContext;
use Hewsda\Security\Foundation\Value\AnonymousKey;
use Hewsda\Security\Foundation\Value\ContextKey;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Hewsda\Security\Foundation\Value\RecallerKey;
use Hewsda\Security\Http\Authorization\DefaultDeniedHandler;
use Hewsda\Security\Http\Entrypoint\GenericFormEntryPoint;

class DefaultFirewallContext implements FirewallContext
{
    /**
     * @var string
     */
    protected $anonymousKey = '1234';

    /**
     * @var string
     */
    protected $providerKey = 'some_provider_key';

    /**
     * @var bool
     */
    protected $anonymous = false;

    /**
     * @var bool
     */
    protected $stateless = false;

    /**
     * @var string
     */
    protected $contextKey = 'some_key';

    /**
     * @var string
     */
    protected $entryPoint = GenericFormEntryPoint::class;

    /**
     * @var string
     */
    protected $deniedHandler = DefaultDeniedHandler::class;

    /**
     * @var string|callable
     */
    protected $logout;

    /**
     * @var
     */
    protected $switchUser;

    /**
     * @var array
     */
    protected $recaller = [];

    public function anonymousKey(): AnonymousKey
    {
        return new AnonymousKey($this->anonymousKey);
    }

    public function setAnonymousKey(string $anonymousKey): FirewallContext
    {
        $this->anonymousKey = $anonymousKey;

        return $this;
    }

    public function providerKey(): ProviderKey
    {
        return new ProviderKey($this->providerKey);
    }

    public function setProviderKey(string $providerKey): FirewallContext
    {
        $this->providerKey = $providerKey;

        return $this;
    }

    public function isAnonymous(): bool
    {
        return $this->anonymous;
    }

    public function setAnonymous(bool $anonymous): FirewallContext
    {
        $this->anonymous = $anonymous;

        return $this;
    }

    public function isStateless(): bool
    {
        return $this->stateless;
    }

    public function setStateless(bool $stateless): FirewallContext
    {
        $this->stateless = $stateless;

        return $this;
    }

    public function contextKey(): ContextKey
    {
        return new ContextKey($this->contextKey);
    }

    public function setContextKey(string $contextKey): FirewallContext
    {
        $this->contextKey = $contextKey;

        return $this;
    }

    public function entryPoint(): ?string
    {
        return $this->entryPoint;
    }

    public function setEntryPoint(string $entryPoint): FirewallContext
    {
        $this->entryPoint = $entryPoint;

        return $this;
    }

    public function deniedHandler(): ?string
    {
        return $this->deniedHandler;
    }

    public function setDeniedHandler(string $deniedHandler): FirewallContext
    {
        $this->deniedHandler = $deniedHandler;

        return $this;
    }

    public function logout()
    {
        return $this->logout;
    }

    public function setLogout($logout): FirewallContext
    {
        if (!is_string($logout) || !is_callable($logout)) {
            throw new FirewallException('Logout service must be a callable or a string.');
        }

        $this->logout = $logout;

        return $this;
    }

    public function switchUser()
    {
    }

    public function setSwitchUser(array $config): FirewallContext
    {
    }

    public function recaller(string $serviceKey): ?RecallerKey
    {
        if ($this->hasRecallerForService($serviceKey)) {
            return new RecallerKey($this->recaller[$serviceKey]);
        }

        return null;
    }

    public function setRecaller(array $recaller): FirewallContext
    {
        $this->recaller = $recaller;

        return $this;
    }

    public function hasRecallerForService(string $serviceKey): bool
    {
        return isset($this->recaller[$serviceKey]);
    }
}