<?php

declare(strict_types=1);

namespace Hewsda\Firewall;

use Hewsda\Firewall\Foundation\Providers\AuthorizationServiceProvider;
use Hewsda\Firewall\Foundation\Providers\BladeDirectiveServiceProvider;
use Hewsda\Firewall\Foundation\Providers\BootstrapServiceProvider;
use Hewsda\Firewall\Foundation\Providers\FirewallServiceProvider as BaseFirewallServiceProvider;
use Hewsda\Firewall\Foundation\Providers\SecurityServiceProvider;
use Hewsda\Firewall\Foundation\Providers\VoterServiceProvider;
use Hewsda\Firewall\Foundation\Support\FirewallConfig;
use Illuminate\Support\ServiceProvider;

class FirewallServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->publishes(
            [__DIR__ . '/../config/firewall.php' => config_path('firewall.php')],
            'firewall');
    }

    public function register(): void
    {
        $this->registerConfiguration();

        $this->registerSecurityBase();

        $this->registerFirewallBase();

        $this->registerLayoutBase();
    }

    protected function registerConfiguration(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/firewall.php', 'firewall');

        $this->app->bind(FirewallConfig::class, function () {
            return new FirewallConfig(config('firewall'));
        });
    }

    protected function registerSecurityBase(): void
    {
        $this->app->register(SecurityServiceProvider::class);
        $this->app->register(AuthorizationServiceProvider::class);
        $this->app->register(VoterServiceProvider::class);
    }

    protected function registerFirewallBase(): void
    {
        $this->app->register(BootstrapServiceProvider::class);
        $this->app->register(BaseFirewallServiceProvider::class);
    }

    protected function registerLayoutBase(): void
    {
        $this->app->register(BladeDirectiveServiceProvider::class);
    }
}