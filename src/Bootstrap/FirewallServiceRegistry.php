<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Bootstrap;

use Hewsda\Firewall\Exception\FirewallException;

class FirewallServiceRegistry
{
    /**
     * @var array
     */
    protected $bootstraps = [];

    /**
     * FirewallRegistry constructor.
     *
     * @param array $bootstraps
     */
    public function __construct(array $bootstraps = [])
    {
        foreach ($bootstraps as $bootstrap) {
            $this->checkRegistry($bootstrap);
        }

        $this->bootstraps = $bootstraps;
    }

    public function addRegistry(array $registry): void
    {
        $this->checkRegistry($registry);

        $this->bootstraps[] = $registry;
    }

    final public function bootstraps(): array
    {
        usort($this->bootstraps, function (array $first, array $next) {
            return array_pop($first) <=> array_pop($next);
        });

        return array_map(function (array $registry) {
            return array_shift($registry);
        }, $this->bootstraps);
    }

    protected function checkRegistry(array $registry): void
    {
        if (2 !== count($registry)) {
            throw new FirewallException('Bootstrap service must be an array of two members with service id and priority');
        }

        [$serviceId, $priority] = $registry;

        if ($this->serviceMustBeUnique($serviceId)) {
            throw new FirewallException(
                sprintf('Firewall registry service "%s" already exists.', $serviceId));
        }

        if (!is_int($priority)) {
            throw new FirewallException(sprintf(
                'No priority set to service registry "%s"', $serviceId));
        }
    }

    protected function serviceMustBeUnique(string $serviceId): bool
    {
        foreach ($this->bootstraps as [$id, $priority]) {
            if ($serviceId === $id) {
                return true;
            }
        }

        return false;
    }
}