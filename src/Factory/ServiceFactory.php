<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Factory;

use Hewsda\Firewall\Exception\FirewallException;
use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Hewsda\Firewall\Foundation\Contracts\Factory\FirewallContext;
use Hewsda\Security\Foundation\Value\ContextKey;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ServiceFactory
{

    /**
     * @var FirewallContext
     */
    private $context;

    /**
     * @var ServiceAggregate
     */
    private $aggregate;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var string
     */
    private $defaultEntrypoint;

    /**
     * @var ContextKey
     */
    private $firewallKey;

    /**
     * @var AuthenticationServices
     */
    private $services;

    /**
     * @var array
     */
    private $userProviderIds;


    public function __construct(ContextKey $firewallKey,
                                AuthenticationServices $services,
                                FirewallContext $context,
                                array $userProviderIds)
    {
        $this->context = $context;
        $this->firewallKey = $firewallKey;
        $this->services = $services;
        $this->userProviderIds = $userProviderIds;
        $this->aggregate = new ServiceAggregate();
    }

    public function make(string $position): Collection
    {
        return $this->services->all()->filter(
            function (AuthenticationServiceFactory $serviceFactory) use ($position) {
                return $position === $serviceFactory->position();
            });
    }

    public function context(): FirewallContext
    {
        return $this->context;
    }

    public function aggregate(): ServiceAggregate
    {
        return $this->aggregate;
    }

    public function __invoke(PayloadFactory $payload): void
    {
        ($this->aggregate)($payload);
    }

    final public function middleware(): array
    {
        return $this->aggregate->listeners();
    }

    public function key(): string
    {
        if (!$this->hasSameContext() && !$this->context()->isStateless()) {
            return $this->context->contextKey()->getKey();
        }

        return $this->firewallKey->getKey();
    }

    public function hasSameContext(): bool
    {
        return $this->context->contextKey()->sameValueAs($this->firewallKey);
    }

    public function originalKey(): ContextKey
    {
        return $this->firewallKey;
    }

    public function setRequest(Request $request): ServiceFactory
    {
        $this->request = $request;

        return $this;
    }

    public function request(): Request
    {
        return $this->request;
    }

    public function defaultEntrypoint(): ?string
    {
        return $this->defaultEntrypoint;
    }

    public function setDefaultEntrypoint(string $defaultEntrypoint): void
    {
        $this->defaultEntrypoint = $defaultEntrypoint;
    }

    public function userProviderIds(): array
    {
        return $this->userProviderIds;
    }

    public function userProvider(string $serviceUserProvider = null): string
    {
        $id = $serviceUserProvider ?? $this->context()->providerKey()->getKey();

        if (!array_key_exists($id, $this->userProviderIds)) {
            throw new FirewallException(
                sprintf('User provider with id "%s" not found', $id));
        }

        return $this->userProviderIds[$id];
    }
}