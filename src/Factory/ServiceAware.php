<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Factory;

use Hewsda\Firewall\Exception\FirewallException;
use Hewsda\Security\Foundation\Value\ContextKey;

/**
 * Class ServiceAware
 * @deprecated
 * @package Hewsda\Firewall\Factory
 */
class ServiceAware
{
    /**
     * @var ContextKey
     */
    private $firewallKey;

    /**
     * @var AuthenticationServices
     */
    private $services;

    /**
     * @var array
     */
    private $userProviderIds;

    /**
     * FirewallAware constructor.
     *
     * @param ContextKey $firewallKey
     * @param AuthenticationServices $services
     * @param array $userProviderIds
     */
    public function __construct(ContextKey $firewallKey, AuthenticationServices $services, array $userProviderIds)
    {
        $this->firewallKey = $firewallKey;
        $this->services = $services;
        $this->userProviderIds = $userProviderIds;
    }

    public function name(): ContextKey
    {
        return $this->firewallKey;
    }


    public function hasUserProvider(string $userProviderId): bool
    {
        return array_key_exists($userProviderId, $this->userProviderIds);
    }

    public function userProvider(string $userProviderId): string
    {
        if (!$this->hasUserProvider($userProviderId)) {
            throw new FirewallException(
                sprintf('User provider with id "%s" not found', $userProviderId));
        }

        return $this->userProviderIds[$userProviderId];
    }

    public function userProviders(): array
    {
        return $this->userProviderIds;
    }
}