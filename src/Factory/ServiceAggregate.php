<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Factory;

use Hewsda\Firewall\Factory\Payload\PayloadFactory;

class ServiceAggregate
{
    /**
     * @var array
     */
    private $listeners = [];

    /**
     * @var array
     */
    private $providers = [];

    /**
     * @var array
     */
    private $entrypoints = [];

    public function __invoke(PayloadFactory $payload): void
    {
        if ($payload->listener()) {
            $this->listeners[] = $payload->listener();
        }

        if ($payload->provider()) {
            $this->providers[] = $payload->provider();
        }

        if ($payload->entrypoint()) {
            $this->entrypoints[] = $payload->entrypoint();
        }
    }

    public function listeners(): array
    {
        return $this->listeners;
    }

    public function providers(): array
    {
        return $this->providers;
    }

    public function entrypoints(): array
    {
        return $this->entrypoints;
    }
}