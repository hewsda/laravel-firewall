<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Factory\Services;

use Hewsda\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory as ServiceFactoryContract;
use Illuminate\Contracts\Container\Container;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

abstract class AuthenticationServiceFactory implements ServiceFactoryContract
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * AuthenticationServiceFactory constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function userProviderKey(): ?string
    {
        return null;
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return null;
    }

    public function isRequired(): bool
    {
        return true;
    }
}