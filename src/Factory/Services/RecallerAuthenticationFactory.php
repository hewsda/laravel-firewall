<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Factory\Services;

use Hewsda\Firewall\Exception\FirewallException;
use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\Payload\PayloadService;
use Hewsda\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Hewsda\Firewall\Foundation\Logout\LogoutManager;
use Hewsda\Firewall\Foundation\Recaller\RecallerManager;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Hewsda\Security\Foundation\Value\RecallerKey;
use Illuminate\Contracts\Container\Container;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

abstract class RecallerAuthenticationFactory implements AuthenticationServiceFactory
{
    /**
     * @var RecallerManager
     */
    protected $recallerManager;

    /**
     * @var LogoutManager
     */
    protected $logoutManager;

    /**
     * @var Container
     */
    protected $container;

    /**
     * AbstractRecallerAuthenticationFactory constructor.
     *
     * @param RecallerManager $recallerManager
     * @param LogoutManager $logoutManager
     * @param Container $container
     */
    public function __construct(RecallerManager $recallerManager, LogoutManager $logoutManager, Container $container)
    {
        $this->recallerManager = $recallerManager;
        $this->logoutManager = $logoutManager;
        $this->container = $container;
    }

    public function register(PayloadService $payload): PayloadFactory
    {
        $recallerKey = $payload->context->recaller($this->serviceKey());

        if (!$recallerKey) {
            throw new FirewallException('A recaller key is mandatory');
        }

        if (!$this->recallerManager->hasService($recallerKey)) {
            throw new FirewallException(
                sprintf('Recaller service with key %s not found', $recallerKey)
            );
        }

        $recallerId = $this->recallerManager->getServiceId($recallerKey);

        $this->logoutManager->addHandler($payload->context->contextKey(), $recallerId);

        return (new PayloadFactory())
            ->setListener($this->registerListener($recallerId))
            ->setProvider($this->registerProvider($payload->firewallKey, $recallerKey));
    }

    abstract public function registerListener(string $recallerServiceId): string;

    abstract public function registerProvider(ProviderKey $providerKey, RecallerKey $recallerKey): string;

    abstract public function serviceKey(): string;

    public function position(): string
    {
        return 'remember_me';
    }

    public function userProviderKey(): ?string
    {
        return null;
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return null;
    }

    public function isRequired(): bool
    {
        return true;
    }
}