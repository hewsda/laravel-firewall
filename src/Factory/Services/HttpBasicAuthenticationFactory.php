<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Factory\Services;

use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\Payload\PayloadService;
use Hewsda\Firewall\Foundation\Http\AuthenticationRequest\HttpBasicAuthenticationRequest;
use Hewsda\Security\Core\Authentication\Provider\GenericFormAuthenticationProvider;
use Hewsda\Security\Foundation\Contracts\Core\User\UserChecker;
use Illuminate\Contracts\Foundation\Application;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

abstract class HttpBasicAuthenticationFactory extends AuthenticationServiceFactory
{
    public function register(PayloadService $payload): PayloadFactory
    {
        $entryPointId = $this->registerEntrypoint($payload);

        return (new PayloadFactory())
            ->setListener($this->registerListener($payload, $entryPointId))
            ->setProvider($this->registerProvider($payload))
            ->setEntrypoint($entryPointId);
    }

    protected function registerProvider(PayloadService $payload): string
    {
        $providerId = 'firewall.' . $this->key() . '_authentication_provider.' . $payload->firewallId();

        $this->container->bind($providerId, function (Application $app) use ($payload) {
            return new GenericFormAuthenticationProvider(
                $payload->firewallKey,
                $app->make($payload->userProviderId),
               $app->make(UserChecker::class),
                $app->make('hash')
            );
        });

        return $providerId;
    }

    abstract public function registerListener(PayloadService $payload, string $entrypointId): string;

    abstract public function registerEntrypoint(PayloadService $payload): string;

    public function position(): string
    {
        return 'http';
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return new HttpBasicAuthenticationRequest();
    }
}