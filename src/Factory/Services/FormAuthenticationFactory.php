<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Factory\Services;

use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\Payload\PayloadService;
use Hewsda\Firewall\Foundation\Recaller\RecallerManager;
use Hewsda\Security\Core\Authentication\Provider\GenericFormAuthenticationProvider;
use Hewsda\Security\Core\User\UserChecker;
use Hewsda\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
use Hewsda\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;

abstract class FormAuthenticationFactory extends AuthenticationServiceFactory
{
    /**
     * @var RecallerManager
     */
    protected $recallerManager;

    /**
     * AbstractFormLoginAuthenticationFactory constructor.
     *
     * @param Container $container
     * @param RecallerManager $recallerManager
     */
    public function __construct(Container $container, RecallerManager $recallerManager)
    {
        parent::__construct($container);

        $this->recallerManager = $recallerManager;
    }

    public function register(PayloadService $payload): PayloadFactory
    {
        $listenerId = $this->registerListener($payload);

        if( $payload->context->hasRecallerForService($this->key())){
            $this->recallerManager->create($payload, $listenerId, $this->key());
        }

        return (new PayloadFactory())
            ->setListener($listenerId)
            ->setProvider($this->registerProvider($payload));
    }

    public function registerProvider(PayloadService $payload): string
    {
        $id = 'firewall.' . $this->position() . '.' . $this->key() . '_authentication_provider.' . $payload->firewallId();

        $this->container->bindIf($id, function (Application $app) use ($payload) {
            return new GenericFormAuthenticationProvider(
                $payload->firewallKey,
                $app->make($payload->userProviderId),
                new UserChecker(),
                $app->make('hash'),
                true
            );
        });

        return $id;
    }

    abstract public function registerListener(PayloadService $payload): string;

    protected function registerAuthenticationSuccess(): string
    {
        $id = 'firewall.' . $this->key() . '.authentication_success_handler';

        $this->container->bindIf($id, DefaultAuthenticationSuccessHandler::class);

        return $id;
    }

    protected function registerAuthenticationFailure(): string
    {
        $id = 'firewall.' . $this->key() . '.authentication_failure_handler';

        $this->container->bindIf($id, DefaultAuthenticationFailureHandler::class);

        return $id;
    }
}