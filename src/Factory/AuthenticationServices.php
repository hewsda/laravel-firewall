<?php

declare(strict_types=1);

namespace Hewsda\Firewall\Factory;

use Hewsda\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Illuminate\Support\Collection;

class AuthenticationServices
{
    /**
     * @var Collection
     */
    private $services;

    /**
     * AuthenticationServices constructor.
     *
     * @param array|null $services
     */
    public function __construct(array $services = null)
    {
        $this->services = new Collection($services ?? []);
    }

    public function add(AuthenticationServiceFactory $service): AuthenticationServices
    {
        $this->services->push($service);

        return $this;
    }

    final public function all(): Collection
    {
        return $this->services;
    }
}