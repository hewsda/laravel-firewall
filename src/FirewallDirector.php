<?php

declare(strict_types=1);

namespace Hewsda\Firewall;

use Hewsda\Firewall\Bootstrap\FirewallServiceRegistry;
use Hewsda\Firewall\Factory\ServiceFactory;
use Illuminate\Contracts\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Collection;

class FirewallDirector
{
    /**
     * @var FirewallServiceRegistry
     */
    private $registry;

    /**
     * @var Pipeline
     */
    private $pipeline;

    /**
     * FirewallDirector constructor.
     *
     * @param FirewallServiceRegistry $registry
     * @param Container $container
     */
    public function __construct(FirewallServiceRegistry $registry, Container $container)
    {
        $this->registry = $registry;
        $this->pipeline = new Pipeline($container);
    }

    public function process(Collection $services, Request $request): Collection
    {
        return $services->map(function (ServiceFactory $factory) use ($request) {
            return $this->pipeline
                ->via('compose')
                ->through($this->registry->bootstraps())
                ->send($factory->setRequest($request))
                ->then(function () use ($factory) {
                    return $factory->middleware();
                });
        });
    }
}